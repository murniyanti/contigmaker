# code to get info of overlapped data of DNA Fragments using swalign
# Notice: swalign python code has been modified for this code.
# info printed to info.txt

# reminder: dictionary have NO order.

import swalign
import re

def rc_seq(seq):
    rev_map = dict(zip("acgtACGTNn-","tgcaTGCANn-"))      # rev_map = reverse map
    return "".join([rev_map[c] for c in seq[::-1]])


scoring = swalign.NucleotideScoringMatrix(1,-3)
sw = swalign.LocalAlignment(scoring, -2)

fo = open("susun.txt", "r")
fw = open("info.txt", "w")

#eachline = fo.readlines()
seq_db = {}
klist=[]

for l in fo:

	if re.match('^\>', l):
		name = l.split()
		key = name[0]
		klist.append(key)
		#print key

	elif re.match('^(a|c|g|t|A|C|G|T)', l):
		#print l
		seq_db[key]=l


	elif re.match(r'^\s*$', l):
		continue

#print lines
print klist
for i in range(len(klist)-1):
	
	#print seq_db[klist[i]], seq_db[klist[i+1]], klist[i], klist[i+1]
	a = sw.align(seq_db[klist[i]], seq_db[klist[i+1]], klist[i], klist[i+1]).dump()
	print >> fw, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]
	'''ab = re.compile('^\>')
			
				if ab.match(eachline[i]):
					r = eachline[i]
					ref = r.split()
					refname = ref[0]
					refseq = eachline[i+1]
			
						#break;
					if i+2 > len(eachline)/2:
						break
			
					else:
						q = eachline[i+2]
						qu = q.split()
						qname = qu[0]
						qseq = eachline[i+3]
			
						a = sw.align(refseq,qseq, refname, qname).dump()
						print >> fw, a
			
				else:
					continue'''

'''# the for loop stop at the last DNA fragment.
# but we still need to align it with the first seq in the Path
# because imagine it is a circle
ref = eachline[-2].split()
refname=ref[0]
refseq = eachline[-1]

qu = eachline[0].split()
qname = q[0]
qseq = eachline[1]
a = sw.align(refseq,qseq, refname, qname).dump()
print >> fw, a'''

a = sw.align(seq_db[klist[-1]], seq_db[klist[0]], klist[-1], klist[0]).dump()
#print type(a)
print >> fw, a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9]

fo.close()
fw.close()


#
#
#
#
# MAKING LAYOUT
#
#
#
#

foinfo = open("info.txt", "r")
fragInfo = foinfo.readlines()
#print type(fragInfo), len(fragInfo)


main_contig = []
offset = 0
currentOrientation = "5p" #>>>> of main_contig
reverse_Orientation = {"5p": "3p", "3p": "5p"}

for i in range( len(fragInfo) ) :

	fragInfo[i].strip()
	
	if i<1:
		continue

	yyy = fragInfo[i].split()
	#print yyy
	rc= yyy[0]
	qname=yyy[1]
	rname=yyy[2]
	qstart=int(yyy[3])
	qend= int(yyy[4])
	len_q= int(yyy[5])
	rstart= int(yyy[6])
	rend= int(yyy[7])
	len_r= int(yyy[8])
	score = int(yyy[9])

	#print rc, qname, rname, qstart, qend, len_q, rstart, rend, len_r, score
	
	#set current fragment
	seq = seq_db[rname]

	if currentOrientation=="3p":
		#that means the main contig is in reverse 
		seq = rc_seq(seq)
	del main_contig[offset:-1]
	main_contig.extend(seq)

	# to set the offset value to merge the next sequence
	if currentOrientation =="5p":
		offset+=qstart
	elif currentOrientation == "3p":
		offset += len_q - qend

	

	'''
	seq = seq_db[qname].strip()
			
	if qstart > qend:
		# means query is reverse complement
		seq = rc_seq(seq)
		del main_contig [rend:offset]
	
	offset = offset + rstart
	del main_contig[offset:rend]
	main_contig.extend(seq)
	'''
	# if rstrand is reverse
	if rstart>rend:
		# rname is reverse compliment
		currentOrientation = reverse_Orientation[currentOrientation]

# out of for loop
j = "";
main_contig=j.join(main_contig)
print "the whole genome:\n\n", main_contig

