

# generate layout without swalign
# based on how the author generate matrix

# input record file
inRecordF = sys.argv[1]

# input DNA Fragments file
DNAFragmentF = sys.argv[2]

FRecord = open(inRecordF, "r")

lines = FRecord.readlines()
#print lines[2]

# Separate lines[2]

p=[]
newstr= lines[1].replace(",", "")
newstr= newstr.replace("[", "")
newstr= newstr.replace("]", "")

x=newstr.split()
for s in x:
	#print s
	if s.isdigit():
		p.append(int(s))

# p is the list of name of DNA fragments that is arranged by the code
print p

# rename the p
for i in p:
	if i>9:
		p = '>frag00{0}'.format(p)
	elif i>99:
		p = '>frag0{0}'.format(p)
	elif i>999:
		p = '>frag{0}'.format(p)


FRecord.close()

# open dna fragment file
FDNAFrag = open(DNAFragmentF, "r")

# Store sequence
seq_db = {}

for l in FDNAFrag:

	if re.match('^\>', l):
		name = l.split()
		key = name[0]

	elif re.match('^(a|c|g|t|A|C|G|T)', l):
		#print l
		seq_db[key]=[ l, name[1], name[2] ]

	elif re.match(r'^\s*$', l):
		continue




# make layout based on no. on the start and end position prepared in the fragment
# file.
offset = 0
offsetEnd = 0


# contig list contain a set of contig
# structure:
# ContigList = [ ['contigname', cStart, cEnd, Seq];
#				 ['contigname', cStart, cEnd, Seq];
#				 ['contigname', cStart, cEnd, Seq] ]
ContigList = []

for FragName in p:	# for i in Arranged fragment
	
	for c in range(len(ContigList)):

		if ContigList[c][1]<= (seq_db[FragName][1] or seq_db[FragName][2]) <= ContigList[c][2]:
			# means within the contig; hence, merge
			

		else:
			ContigList.append(['contig{0}'.format(len(ContigList)+1), seq_db[FragName][1], seq_db[FragName][2], seq_db[FragName][3]])

	