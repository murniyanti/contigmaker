

# not finished
















import re

seq_db = {}

# open info file

overlapdata = open('info_m15421_5.dat', 'r')
odata = overlapdata.readlines()

fragmentlist = open('frag_m15421_5.dat', 'r')


# store fragment in seq_db dictionary
for i in fragmentlist:

	if re.match('^\>', i):
		name = i.split()
		key = name[0]
		#klist.append(key)
		#print key

	elif re.match('^(a|c|g|t|A|C|G|T)', i):
		#print i
		seq_db[key]=i


	elif re.match(r'^\s*$', i):
		continue

mainConsensus = []
offset = 0

for i in range(len(odata)-1):

	odata[i].strip()

	yyy = odata[i].split()

	qname=yyy[1]
	rname=yyy[2]
	qstart=int(yyy[3])
	qend= int(yyy[4])
	len_q= int(yyy[5])
	rstart= int(yyy[6])
	rend= int(yyy[7])
	len_r= int(yyy[8])


	offset = offset + rstart

	# if the first fragment from the left
	if i<1: 
		mainConsensus.extend(seq_db[rname][:])

		if len(mainConsensus[:offset]) > len(seq_db[qname][:qstart]) and len(mainConsensus[ (offset+(len(seq_db[qname][qstart:qend])) ) : ]) > len(seq_db[qname][qend:]) :
			del mainConsensus[offset:]
			mainConsensus.extend(seq_db[qname][qstart:])
			#reset offset
			offset = offset - len(seq_db[qname][:qstart])
			continue

		elif len(mainConsensus[:offset]) > len(seq_db[qname][:qstart]) and len(mainConsensus[offset:]) > len(seq_db[qname][qend:]) :
			#reset offset
			offset = offset - len(seq_db[:qstart])
			continue

		elif len(seq_db[qname][:qstart]) > len(mainConsensus[:offset]) and len(mainConsensus[(offset+(len(seq_db[qname][qstart:qend])) ) : ]) > len(seq_db[qname][qend:]):
			mainConsensus[0:0] = seq_db[qname][:qstart]
			#reset offset
			offset = offset - len(seq_db[:qstart])
			continue

		elif len(seq_db[qname][:qstart]) > len(mainConsensus[:offset]) and len(seq_db[qname][qend:]) > len(mainConsensus[(offset+(qend-qstart )):]):
			print "{0}", format(type(mainConsensus))
			print type(seq_db[qname][:])
			mainConsensus = seq_db[qname][:]
			offset = 0
			'''
			offset = offset + rstart
			del mainConsensus[offset:]
			mainConsensus.extend(seq_db[qname][qstart:qend])
			'''
			continue


	if len(mainConsensus[:offset]) > len(seq_db[qname][:qstart]) and len(mainConsensus[ (offset+(len(seq_db[qname][qstart:qend])) ) : ]) > len(seq_db[qname][qend:]) :
		
		del mainConsensus[offset:]
		mainConsensus.extend(seq_db[qname][qstart:])
		#reset offset
		offset = offset - len(seq_db[qname][:qstart])

	elif len(mainConsensus[:offset]) > len(seq_db[qname][:qstart]) and len(mainConsensus[offset:]) > len(seq_db[qname][qend:]) :
		#reset offset
		offset = offset - len(seq_db[qname][:qstart])

	elif len(seq_db[qname][:qstart]) > len(mainConsensus[:offset]) and len(mainConsensus[(offset+(len(seq_db[qname][qstart:qend])) ) : ]) > len(seq_db[qname][qend:]):
		leftoverhang = seq_db[qname][:qstart]
		mainConsensus[0:0] = leftoverhang
		#reset offset
		offset = offset - len(seq_db[qname][:qstart])

	elif len(seq_db[qname][:qstart]) > len(mainConsensus[:offset]) and len(seq_db[qname][qend:]) > len(mainConsensus[(offset+(len(seq_db[qname][qstart:qend])) )]):
		mainConsensus = seq_db[qname][:]
		offset = 0


			





	
	#offset = offset - len(seq_db[qname][:qstart])



lastFrag = odata[-1].strip()
yyy = lastFrag.split()

qname=yyy[1]
rname=yyy[2]
qstart=int(yyy[3])
qend= int(yyy[4])
len_q= int(yyy[5])
rstart= int(yyy[6])
rend= int(yyy[7])
len_r= int(yyy[8])


offset = offset + rstart


if len(mainConsensus[:offset]) > len(seq_db[qname][:qstart]) and len(mainConsensus[ (offset+(len(seq_db[qname][qstart:qend])) ) : ]) > len(seq_db[qname][qend:]) :
	del mainConsensus[offset:]
	mainConsensus.extend(seq_db[qname][qstart:])
	#reset offset
	offset = offset - len(seq_db[:qstart])

elif len(mainConsensus[:offset]) > len(seq_db[qname][:qstart]) and len(mainConsensus[offset:]) > len(seq_db[qname][qend:]) :
	#reset offset
	offset = offset - len(seq_db[:qstart])

elif len(seq_db[qname][:qstart]) > len(mainConsensus[:offset]) and len(mainConsensus[(offset+(len(seq_db[qname][qstart:qend])) ) : ]) > len(seq_db[qname][qend:]):
	mainConsensus[0:0] = seq_db[qname][:qstart]
	#reset offset
	offset = offset - len(seq_db[:qstart])

elif len(seq_db[qname][:qstart]) > len(mainConsensus[:offset]) and len(seq_db[qname][qend:]) > len(mainConsensus[(offset+(len(seq_db[qname][qstart:qend])) )]):
	mainConsensus = seq_db[qname][:]
	offset = 0


mainConsensus = "".join(mainConsensus)
mainConsensus = "".join(mainConsensus.split())

writeConsensus = open("result.fa", "w")
print mainConsensus
print >>writeConsensus, mainConsensus

overlapdata.close()
fragmentlist.close()
writeConsensus.close()


'''
overlap have 4 conditions
1- A dovetails to B

A |-----------------------------|
					|------------------------| B


2- A contains B

A  |-----------------------------|
			|-----------|	B

3- B dovetails to A

				|----------------------------| A
	B |--------------------------|


4- B contains A

			|----------------| A
	B |----------------------------------|

'''