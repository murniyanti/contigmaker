import re
import linecache
import sys

# Sample input:
#Best Structure= [35, 17, 28, 0, 10, 46, 31, 3, 11, 41, 47, 24, 38, 19, 1, 23, 6, 36, 27, 4, 32, 37, 2, 44, 12, 29, 34, 14, 43, 5, 45, 39, 9, 33, 18, 22, 40, 13, 25, 7, 15, 16, 20, 8, 30, 42, 21, 26]
infile = sys.argv[1]
infile2 = sys.argv[2]
fo = open(infile, "r")

lines = fo.readlines()
#print lines[2]

# Separate lines[2]

p=[]
newstr= lines[0].replace(",", "")
newstr= newstr.replace("[", "")
newstr= newstr.replace("]", "")

x=newstr.split()
for s in x:
	#print s
	if s.isdigit():
		p.append(int(s))

# p is the list of name of DNA fragments that is arranged by the code
print p

fo.close()

#-----------
#Open fragment file

#fi = open("frag_x60189_4.dat", "r")
fo = open("x4newArr.txt", "w")

fragfile = 'frag_x60189_4.dat'

for es in p:

	l=linecache.getline(fragfile, ((es*2)+1) )
	l = l.strip()
	print>>fo, l

	l=linecache.getline(fragfile, ((es*2)+1+1) )
	l = l.strip()
	print>>fo, l

fo.close()

